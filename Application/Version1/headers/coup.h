/**
 * @file coup.h
 * @brief Représentation des coups d'une partie
 * @author BRES Maxence - BETEND Baptiste
 *
 * Types et fonctions permettant de manipuler les coups d'une partie, sous forme de liste chainée.
 *
 */

#ifndef COUP_H

#define COUP_H 

#include "echiquier.h"
#include "pieces.h"
#include <stdlib.h>
#include <stdio.h>
/**
 * @struct coup_t
 * @brief la représentation des coups
 * 
 * Cette structure contient un champ entier qui est le numéro du coup, un champ couleur_t qui représente le joueur ayant joué le coup,
 * un champ de type echiquier_t qui contient l'état de l'échiquier à la fin du coup, et un champ 
 */
typedef struct coup{
	int numero;
	couleur_t couleur;
	echiquier_t echiquier;
	char* commentaire;
	struct coup *coup_suivant;
}coup_t;

/**
 * @fn         void copier_echiquier (echiquier_t echiquier_sortie, echiquier_t echiquier_a_copier)
 * @brief      copie un échiquier dans un autre échiquier
 *
 * @param[in]  echiquier_sortie    L'échiquier dans lequel on copie les nouvelles valeurs
 * @param[in]  echiquier_a_copier  L'échiquier dont on prend les valeurs pour les copier dans l'autre.
 */
void copier_echiquier (echiquier_t echiquier_sortie, echiquier_t echiquier_a_copier);

/**
 * @fn         void initilaiser_echiquier(echiquier_t echiquer)
 * @brief      Initialise un échiquier avec les valeurs de début de partie
 *
 * @param[in]  echiquier  L'échiquier a initialiser
 */
void initialiser_echiquier(echiquier_t echiquier);

/**
 * @fn         int verifier_coup (char* string, coup_t* coup)
 * @brief      Verifie si un coup est valide
 *
 * @param      string   une chaine de caractère représentant le coup à tester
 * @param      coup     un coup_t contenant l'état actuel de la partie
 *
 * @return     1 si le coup est valide, 0 sinon.
 */
int verifier_coup (char* string, coup_t* coup);

/**
 * @fn         void creer_coup (char* string, coup_t* coup)
 * @brief      Rajoute un nouveau coup dans notre liste chainée
 *
 * @param      string  une chaine de caractère représentant le coup à jouer
 * @param      coup    un coup_t contenant l'état actuel de la partie
 */
void creer_coup (char* string, coup_t* coup);

#endif
