/**
 * @file echiquier.h
 * @brief Représentation de l'échiquier
 * @author BRES Maxence - BETEND Baptiste
 *
 * Types et fonctions permettant de manipuler des cases, et des échiquiers qui sont des tableaux de cases.
 *
 */

#ifndef ECHIQUIER_H

#define ECHIQUIER_H 

#include "pieces.h"

/**
 * @struct case_t
 * @brief Les cases de l'échiquier
 * 
 * Cette structure contient deux champs, piece et couleur, qui
 * représente respectivement la pièce présente sur la case et sa couleur
 */
typedef struct Case {
	couleur_t couleur;
	piece_t piece;
}case_t;

/**
 *@brief Echiquier, tableau de case_t
 *
 * Le type echiquier_t est un tableau à deux dimensions
 * 8x8 de case_t
 */
typedef case_t echiquier_t[8][8];

/**
 * @fn         case_t case_t_de_pc(piece_t piece, couleur_t couleur)
 * @brief      Génération d'une case_t
 *
 * @param[in]  piece	la piece de type piece_t à placer dans la case
 * @param[in]  couleur  la couleur de la piece placée dans la case
 *
 * @return     Une case_t dont les champs ont pris les valeurs des paramètres d'entrée de la fonction
 */
case_t case_t_de_pc(piece_t piece, couleur_t couleur);

/**
 * @fn         piece_t piece_t_de_case_t(case_t cases)
 * @brief      Retourne la pièce d'une case
 *
 * @param[in]  cases  la case dans laquelle on va récupérer le champ 'piece'
 *
 * @return     La piece de type piece_t, contenue dans la case donnée en entrée de la fonction
 */
piece_t piece_t_de_case_t(case_t cases);

/**
 * @fn         piece_t couleur_t_de_case_t(case_t cases)
 * @brief      Retourne la couleur d'une case
 *
 * @param[in]  cases  la case dans laquelle on va récupérer le champ 'couleur'
 *
 * @return     La couleur de type couleur_t, contenue dans la case donnée en entrée de la fonction
 */
couleur_t couleur_t_de_case_t(case_t cases);

/**
 * @fn         void set_case(echiquier_t echiquier, int ligne, int colonne, case_t cases)
 * @brief      Affecte du contenu à une case
 *
 * @param[in]  echiquier  l'échiquier sur lequel on va affecter une case
 * @param[in]  ligne      la ligne de la case à affecter
 * @param[in]  colonne    la colonne de la case à affecter
 * @param[in]  cases      la case qui va être affectée aux coordonnées ligne et colonne
 */
void set_case(echiquier_t echiquier, int ligne, int colonne, case_t *cases);

/**
 * @fn         void get_case(echiquier_t echiquier, int ligne, int colonne, case_t *presse_papier)
 * @brief      Récupère une copie d'une case
 *
 * @param[in]  echiquier      l'échiquier sur lequel on va chercher la case à copier
 * @param[in]  ligne          la ligne de la case à copier
 * @param[in]  colonne        la colonne de la case à copier
 * @param      presse_papier  un pointeur vers la case qui va récupérer les valeurs de la case à copier
 */
void get_case(echiquier_t echiquier, int ligne, int colonne, case_t *presse_papier);

/**
 * @fn         int indice_de_ligne (char caractere)
 * @brief      Traduit le nom d'une ligne en indice
 *
 * @param[in]  caractere  le nom de la ligne à traduire, compris entre '1' et '8'
 *
 * @return     un entier entre 0 et 7
 */
int indice_de_ligne (char caractere);

/**
 * @fn         char ligne_de_indice (int indice)
 * @brief      Traduit un indice en un nom de ligne
 *
 * @param[in]  indice  l'indice à traduire en nom de ligne, compris entre 0 et 7
 *
 * @return     un caractère entre '1' et '8'
 */
char ligne_de_indice (int indice);

/**
 * @fn         int indice_de_colonne (char caractere)
 * @brief      Traduit le nom de d'une colonne en indice 
 *
 * @param[in]  caractere  le nom de la colonne à traduire, compris entre 'A' et 'H'
 *
 * @return     un entier entre 0 et 7
 */
int indice_de_colonne (char caractere);

/**
 * @fn         char colonne_de_indice (int indice)
 * @brief      Traduit un indice en nom de colonne
 *
 * @param[in]  indice  l'indice à traduire en nom de colonne, compris entre 0 et 7
 *
 * @return     un entier entre 0 et 7
 */
char colonne_de_indice (int indice);

/**
 * @fn         int char_ligne_valide (char caractere)
 * @brief      Vérifie la validité d'un nom de ligne
 *
 * @param[in]  caractere  un caractère qui est le nom de la ligne à vérifier
 *
 * @return     1 si le nom est valide, 0 sinon
 */
int char_ligne_valide (char caractere);

/**
 * @fn         int char_colonne_valide (char caractere)
 * @brief      Vérifie la validité d'un nom de colonne
 *
 * @param[in]  caractere  un caractère qui est le nom de la colonne à vérifier
 *
 * @return     1 si le nom est valide, 0 sinon
 */
int char_colonne_valide (char caractere);

#endif
