/**
 * @file pieces.h
 * @brief Représentation des pièces d'échec
 * @author BRES Maxence - BETEND Baptiste
 *
 * Types et fonctions permettant de manipuler des pièces d'échec et leur couleur.
 *
 */

#ifndef PIECES_H

#define PIECES_H 

/**
 * @enum couleur_t
 * @brief Couleurs des pièces d'échec
 * 
 * 
 */
typedef enum couleur {
	BLANC, NOIR, AUCUNE
}couleur_t;

/**
 * @enum piec_t
 * @brief Pieces d'échec
 * 
 * piece_t est la liste de toutes les pièces aux échecs
 * contenant également les cases vides et les pièces autres
 */
typedef enum piece {
	PION, TOUR, CAVALIER, FOU, DAME, ROI, VIDE, AUTRE
} piece_t;

/**
 * @fn         char lettre_de_piece(piece_t piece)
 * @brief      Convertit une pièce d'échec en un caractère
 *
 * @param[in]  piece , la pièce à convertir en caractère
 *
 * @return     Le caractère correspondant à la pièce entrée en paramètre
 */
char lettre_de_piece(piece_t piece);

/**
 * @fn         piece_t piece_de_lettre(char piece)
 * @brief      Convertit un caractère en une pièce d'échec
 *
 * @param[in]  piece, le caractère à convertir en type pièce (piece_t)
 *
 * @return     La pièce correspondante au caractère entré en paramètre
 */
piece_t piece_de_lettre(char piece);

#endif
