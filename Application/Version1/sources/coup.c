/**
 * @file coup.c
 * @brief Représentation des coups d'une partie
 * @author BRES Maxence - BETEND Baptiste
 *
 * Types et fonctions permettant de manipuler les coups d'une partie, sous forme de liste chainée.
 *
 */

#include "coup.h"

void copier_echiquier(echiquier_t echiquier_sortie, echiquier_t echiquier_a_copier)
{
    for (int i = 0; i < 8; ++i) {
        for (int j = 0; j < 8; ++j) {
            echiquier_sortie[i][j] = echiquier_a_copier[i][j];
        }
    }
}

void initialiser_echiquier(echiquier_t echiquier)
{

    // Placement des pions et des cases vides
    for (int i = 0; i < 8; ++i) {
        echiquier[1][i] = case_t_de_pc(PION, BLANC);
        echiquier[2][i] = case_t_de_pc(VIDE, AUCUNE);
        echiquier[3][i] = case_t_de_pc(VIDE, AUCUNE);
        echiquier[4][i] = case_t_de_pc(VIDE, AUCUNE);
        echiquier[5][i] = case_t_de_pc(VIDE, AUCUNE);
        echiquier[6][i] = case_t_de_pc(PION, NOIR);
    }

    // Les tours
    echiquier[0][0] = case_t_de_pc(TOUR, BLANC);
    echiquier[0][7] = case_t_de_pc(TOUR, BLANC);
    echiquier[7][0] = case_t_de_pc(TOUR, NOIR);
    echiquier[7][7] = case_t_de_pc(TOUR, NOIR);

    // Les cavaliers
    echiquier[0][1] = case_t_de_pc(CAVALIER, BLANC);
    echiquier[0][6] = case_t_de_pc(CAVALIER, BLANC);
    echiquier[7][1] = case_t_de_pc(CAVALIER, NOIR);
    echiquier[7][6] = case_t_de_pc(CAVALIER, NOIR);

    // Les fous
    echiquier[0][2] = case_t_de_pc(FOU, BLANC);
    echiquier[0][5] = case_t_de_pc(FOU, BLANC);
    echiquier[7][2] = case_t_de_pc(FOU, NOIR);
    echiquier[7][5] = case_t_de_pc(FOU, NOIR);

    // Les rois et dames
    echiquier[0][3] = case_t_de_pc(DAME, BLANC);
    echiquier[0][4] = case_t_de_pc(ROI, BLANC);
    echiquier[7][3] = case_t_de_pc(DAME, NOIR);
    echiquier[7][4] = case_t_de_pc(ROI, NOIR);
}

int verifier_coup(char* string, coup_t* coup)
{
    // Les cases existent
    int cond1 = char_colonne_valide(string[0]) && char_ligne_valide(string[1]) && char_colonne_valide(string[2]) && char_ligne_valide(string[3]);
    // Les cases sont différentes
    int cond2 = string[0] != string[2] || string[1] != string[3];
    // La case de départ n'est pas vide et de la bonne couleur
    case_t* pp = malloc(sizeof(case_t));
    get_case(coup->echiquier, indice_de_ligne(string[1]), indice_de_colonne(string[0]), pp);
    int cond3 = pp->piece != VIDE && pp->couleur == coup->couleur;
    // La case d'arrivée ne contient pas une de nos pièces
    get_case(coup->echiquier, indice_de_ligne(string[3]), indice_de_colonne(string[2]), pp);
    int cond4 = pp->couleur != coup->couleur;
    free(pp);
    return cond1 && cond2 && cond3 && cond4;
}

void creer_coup(char* string, coup_t* coup)
{
    coup_t* nouveau_coup = malloc(sizeof(coup_t));
    nouveau_coup->numero = coup->numero + 1;
    switch (coup->couleur) {
    case BLANC :
        nouveau_coup->couleur = NOIR;
        break;
    case NOIR:
        nouveau_coup->couleur = BLANC;
        break;
    case AUCUNE:
        break;
    }
    if (verifier_coup(string, coup)) {
        // On modifie l'échiquier
        case_t* nouvelle_case = malloc(sizeof(case_t));
        get_case(coup->echiquier, indice_de_ligne(string[1]), indice_de_colonne(string[0]), nouvelle_case);
        set_case(coup->echiquier, indice_de_ligne(string[3]), indice_de_colonne(string[2]), nouvelle_case);
        coup->echiquier[indice_de_ligne(string[1])][indice_de_colonne(string[0])] = case_t_de_pc(VIDE, AUCUNE);
        coup->coup_suivant = nouveau_coup;
        copier_echiquier(nouveau_coup->echiquier, coup->echiquier);
        return;
    } else {
        printf("coup numéro %i non valide !\n", coup->numero);
    }
}
