/**
 * @file main.c
 * @brief Main
 * @author BRES Maxence - BETEND Baptiste
 *
 * Le main qui lit des chaines de caractères depuis un fichier ou depuis le terminal pour les jouer.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "pieces.h"
#include "echiquier.h"
#include "coup.h"
#include "vt100.h"

int main(int argc, char *argv[]) {

// On crée ici le coup initial qui est un coup blanc de numéro 1
	coup_t* coup_initial = malloc(sizeof(coup_t));
	echiquier_t echiquier_initial;
	initialiser_echiquier(echiquier_initial);
	coup_initial->numero = 1;
	coup_initial->couleur = BLANC;
	copier_echiquier(coup_initial->echiquier, echiquier_initial);

	coup_t* teteDeListe = coup_initial; // Le pointeur sur le début de la liste de coups.
	coup_t* queueDeListe = coup_initial; // Le pointeur sur la fin de la liste de coups.

	FILE *fd; // Le file description pointe soit l'entrée standard soit un fichier

	char bufferCoup[4 + 1]; // Stocke les mouvements 1 par 1, on stocke aussi le séparateur

	if (argc > 2) {
		fprintf(stderr, "Erreur : trop d'arguments. utilisation par nom de fichier ou entrée clavier\n");
		exit(1);
	}
	if (argc == 1) {
		printf("Entrez les coups sous la forme suivante : E2E4\nEt entrez CTRL+D pour terminer \n");
		printf("Entrée au clavier : \n");
		fd = stdin;
	}
	else if (argc == 2) {
		printf("Entrée par fichier : \n");
		fd = fopen(argv[1], "r"); // r pour accès en lecture
		if (fd == NULL) {
			fprintf(stderr, "impossible d'ouvrir le fichier\n");
			exit(0);
		}
	}

	while (fscanf(fd, "%s", bufferCoup) != EOF) {
		printf("%s\n", bufferCoup);
		creer_coup(bufferCoup, queueDeListe);
		queueDeListe = queueDeListe->coup_suivant;
	}

	if (argc == 2) {
		fclose(fd); // si on lisait depuis un fichier, on ferme le descripteur
	}

	return 0;
}