/**
 * @file pieces.c
 * @brief Représentation des pièces d'échec
 * @author BRES Maxence - BETEND Baptiste
 *
 * Types et fonctions permettant de manipuler des pièces d'échec et leur couleur.
 *
 */
#include "pieces.h"

char lettre_de_piece(piece_t piece) {
	switch(piece){
		case PION : return 'p';
		case TOUR : return 'T';
		case CAVALIER : return 'C';
		case FOU : return 'F';
		case DAME : return 'D';
		case ROI : return 'R';
		case VIDE : return ' ';
		case AUTRE : return '*';
		default : return 'Z';
	}
}

piece_t piece_de_lettre(char piece){
	switch(piece){
		case 'p' : return PION;
		case 'T' : return TOUR;
		case 'C' : return CAVALIER;
		case 'F' : return FOU;
		case 'D' : return DAME;
		case 'R' : return ROI;
		case ' ' : return VIDE;
		default : return AUTRE;
	}

}