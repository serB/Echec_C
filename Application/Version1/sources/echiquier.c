/**
 * @file echiquier.c
 * @brief Représentation de l'échiquier
 * @author BRES Maxence - BETEND Baptiste
 *
 * Types et fonctions permettant de manipuler des cases, et des échiquiers qui sont des tableaux de cases.
 *
 */

#include "echiquier.h"

case_t case_t_de_pc(piece_t piece, couleur_t couleur){
	case_t cases;
	cases.couleur = couleur;
	cases.piece = piece;
	return cases;
}

piece_t piece_t_de_case_t(case_t cases){
	return cases.piece;
}

couleur_t couleur_t_de_case_t(case_t cases){
	return cases.couleur;
}
void set_case(echiquier_t echiquier, int ligne, int colonne, case_t *cases){
	echiquier[ligne][colonne] = *cases;
}
void get_case(echiquier_t echiquier, int ligne, int colonne, case_t *presse_papier){
	*presse_papier = echiquier[ligne][colonne];
}

int indice_de_ligne (char caractere) {
	return caractere-'1';
}

char ligne_de_indice (int indice) {
	return '1'+indice ;
}

int indice_de_colonne (char caractere) {
	return caractere-'A';
}

char colonne_de_indice (int indice) {
	return 'A'+indice ;
}

int char_ligne_valide (char caractere) {
	return caractere >= '1' && caractere <= '8' ;
}

int char_colonne_valide (char caractere) {
	return caractere >= 'A' && caractere <= 'H' ;
}

